/*
Copyright (©) 2003-2022 Teus Benschop.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#pragma once

#include <config/libraries.h>

class Database_Cache
{
public:
  static void create (string resource, int book);
  static void remove (string resource);
  static void remove (string resource, int book);
  static bool exists (string resource);
  static bool exists (string resource, int book);
  static bool exists (string resource, int book, int chapter, int verse);
  static void cache (string resource, int book, int chapter, int verse, string value);
  static string retrieve (string resource, int book, int chapter, int verse);
  static int count (string resource);
  static bool ready (string resource, int book);
  static void ready (string resource, int book, bool ready);
  static int size (string resource, int book);
  static string fragment ();
  static string path (string resource, int book);
private:
  static string filename (string resource, int book);
};


bool database_filebased_cache_exists (string schema);
void database_filebased_cache_put (string schema, string contents);
string database_filebased_cache_get (string schema);
void database_filebased_cache_remove (string schema);
string database_filebased_cache_name_by_ip (string address, string id);
string database_filebased_cache_name_by_session_id (string sid, string id);


string focused_book_filebased_cache_filename (string sid);
string focused_chapter_filebased_cache_filename (string sid);
string focused_verse_filebased_cache_filename (string sid);
string general_font_size_filebased_cache_filename (string sid);
string menu_font_size_filebased_cache_filename (string sid);
string resource_font_size_filebased_cache_filename (string sid);
string hebrew_font_size_filebased_cache_filename (string sid);
string greek_font_size_filebased_cache_filename (string sid);
string greek_font_size_filebased_cache_filename (string sid);
string current_theme_filebased_cache_filename (string sid);


void database_cache_trim (bool clear);


bool database_cache_can_cache (const string & error, const string & html);
